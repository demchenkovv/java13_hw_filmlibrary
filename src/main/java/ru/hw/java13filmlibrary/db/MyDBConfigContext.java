//package ru.hw.java13filmlibrary.db;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//
//import static ru.hw.java13filmlibrary.db.constants.DBConstants.*;
//
//@Configuration
//@ComponentScan()
//public class MyDBConfigContext {
//
//    @Bean
//    @Scope("singleton")
//    public Connection newConnection() throws SQLException {
//        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB, USER, PASSWORD);
//    }
//}
