package ru.hw.java13filmlibrary.filmLibrary.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class OrderDTO
        extends GenericDTO {

    private Long userId;
    private Long filmId;
    private LocalDate rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
}
