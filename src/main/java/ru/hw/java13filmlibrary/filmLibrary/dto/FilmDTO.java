package ru.hw.java13filmlibrary.filmLibrary.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.hw.java13filmlibrary.filmLibrary.model.Country;
import ru.hw.java13filmlibrary.filmLibrary.model.Genre;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO
        extends GenericDTO {

    private String title;
    private String premierYear;
    private Country country;
    private Genre genre;
    private Set<Long> directorsIds;
    private String onlineCopyPath;
}
