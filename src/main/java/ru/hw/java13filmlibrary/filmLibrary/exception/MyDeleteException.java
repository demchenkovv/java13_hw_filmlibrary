package ru.hw.java13filmlibrary.filmLibrary.exception;

public class MyDeleteException
        extends Exception {

    public MyDeleteException(String message) {
        super(message);
    }
}
