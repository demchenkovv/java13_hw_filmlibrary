package ru.hw.java13filmlibrary.filmLibrary.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.hw.java13filmlibrary.filmLibrary.dto.GenericDTO;
import ru.hw.java13filmlibrary.filmLibrary.mapper.GenericMapper;
import ru.hw.java13filmlibrary.filmLibrary.model.GenericModel;
import ru.hw.java13filmlibrary.filmLibrary.repository.GenericRepository;

import java.util.List;

/**
 * Класс должен быть - АБСТРАКТНЫМ
 * Всегда работаем с DTOшками, так как DTOшка уже реализована
 * для конкретного класса и не нужно каждый раз удалять ненужные
 * поля и зависимости, поэтому при создании тоже используем DTO
 *
 * @param <E> - Entity
 * @param <D> - DTO
 */
@Service
public abstract class GenericService<E extends GenericModel, D extends GenericDTO> {
    protected final GenericRepository<E> repository;
    protected final GenericMapper<E, D> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<E> repository,
                             GenericMapper<E, D> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<D> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public D getOne(Long id) {
        return mapper.toDTO(repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Объект не найден по данному id=" + id)));
    }

    public D create(D object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public D update(D object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
