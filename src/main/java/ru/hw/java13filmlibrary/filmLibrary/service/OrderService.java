package ru.hw.java13filmlibrary.filmLibrary.service;

import org.springframework.stereotype.Service;
import ru.hw.java13filmlibrary.filmLibrary.dto.FilmDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.OrderDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.UserDTO;
import ru.hw.java13filmlibrary.filmLibrary.mapper.OrderMapper;
import ru.hw.java13filmlibrary.filmLibrary.model.Film;
import ru.hw.java13filmlibrary.filmLibrary.model.Order;
import ru.hw.java13filmlibrary.filmLibrary.repository.OrderRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService
        extends GenericService<Order, OrderDTO> {

    private UserService userService;
    private FilmService filmService;
    private OrderRepository orderRepository;

    protected OrderService(OrderRepository orderRepository,
                           OrderMapper orderMapper,
                           UserService userService,
                           FilmService filmService) {
        super(orderRepository, orderMapper);
        this.userService = userService;
        this.filmService = filmService;
        this.orderRepository = orderRepository;
    }

    public OrderDTO rentFilm(Long userId, Long filmId, Integer rentPeriod) {

        UserDTO userDTO = userService.getOne(userId);
        FilmDTO filmDTO = filmService.getOne(filmId);

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setCreatedWhen(LocalDateTime.now());
        orderDTO.setUserId(userDTO.getId());
        orderDTO.setFilmId(filmDTO.getId());
        orderDTO.setPurchase(false);
        orderDTO.setRentDate(LocalDate.now());
        if (rentPeriod != 0) {
            orderDTO.setRentPeriod(rentPeriod);
        } else {
            orderDTO.setRentPeriod(14);
        }
        return mapper.toDTO(repository.save(mapper.toEntity(orderDTO)));
    }

    public OrderDTO buyFilm(Long userId, Long filmId) {
        UserDTO userDTO = userService.getOne(userId);
        FilmDTO filmDTO = filmService.getOne(filmId);

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setCreatedWhen(LocalDateTime.now());
        orderDTO.setUserId(userDTO.getId());
        orderDTO.setFilmId(filmDTO.getId());
        orderDTO.setPurchase(true);

        return mapper.toDTO(repository.save(mapper.toEntity(orderDTO)));
    }

    /**
     * Вернуть список всех когда-либо арендованных или купленных фильмов у пользователя
     */
    public List<FilmDTO> getAllFilmsByUserId(Long userId) {
        List<Film> orderList = orderRepository
                .findByUserId(userId)
                .stream()
                .map(Order::getFilm)
                .toList();
        return filmService.mapper.toDTOs(orderList);
    }

    /**
     * Вернуть список всех ДЕЙСТВУЮЩИХ арендованных или купленных фильмов у пользователя
     */
    public List<FilmDTO> getCurrentFilmsByUserId(Long userId) {
        List<Film> orderList = orderRepository
                .findByUserId(userId)
                .stream()
                .filter(o -> o.getPurchase() || LocalDate.now().isBefore(o.getRentDate().plusDays(o.getRentPeriod())))
                .map(Order::getFilm)
                .toList();
        return filmService.mapper.toDTOs(orderList);
    }
}


//        Данный метод возвращает OrderDTO по userId
//        public List<OrderDTO> getAllFilmsByUserId(Long userId) {
//        return mapper.toDTOs(orderRepository.findByUserId(userId));
//    }