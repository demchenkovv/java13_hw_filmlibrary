package ru.hw.java13filmlibrary.filmLibrary.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.hw.java13filmlibrary.filmLibrary.dto.DirectorDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.DirectorWithFilmsDTO;
import ru.hw.java13filmlibrary.filmLibrary.mapper.DirectorMapper;
import ru.hw.java13filmlibrary.filmLibrary.mapper.DirectorWithFilmsMapper;
import ru.hw.java13filmlibrary.filmLibrary.model.Director;
import ru.hw.java13filmlibrary.filmLibrary.model.Film;
import ru.hw.java13filmlibrary.filmLibrary.repository.DirectorRepository;
import ru.hw.java13filmlibrary.filmLibrary.repository.FilmRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class DirectorService
        extends GenericService<Director, DirectorDTO> {

    private final FilmRepository filmRepository;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;

    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper,
                              FilmRepository filmRepository,
                              DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(directorRepository, directorMapper);
        this.filmRepository = filmRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
    }

    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Director director = repository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Режиссер не найден с id=" + directorId));
        Film film = filmRepository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильм не найден с id=" + filmId));
        director.getFilms().add(film);
        return mapper.toDTO(repository.save(director));
    }

    public List<DirectorWithFilmsDTO> listAllDirectorsWithFilms() {
        return directorWithFilmsMapper.toDTOs(repository.findAll());
    }

    public List<DirectorWithFilmsDTO> getDirectorsByFilmId(Long filmId) {
        Film film = filmRepository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильм не найден с id=" + filmId));
        List<Director> directorsList = new ArrayList<>(film.getDirectors());
        return directorWithFilmsMapper.toDTOs(directorsList);
    }
}