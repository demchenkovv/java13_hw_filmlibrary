package ru.hw.java13filmlibrary.filmLibrary.service;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import ru.hw.java13filmlibrary.filmLibrary.dto.FilmDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.FilmWithDirectorsDTO;
import ru.hw.java13filmlibrary.filmLibrary.mapper.FilmMapper;
import ru.hw.java13filmlibrary.filmLibrary.mapper.FilmWithDirectorsMapper;
import ru.hw.java13filmlibrary.filmLibrary.model.Director;
import ru.hw.java13filmlibrary.filmLibrary.model.Film;
import ru.hw.java13filmlibrary.filmLibrary.repository.DirectorRepository;
import ru.hw.java13filmlibrary.filmLibrary.repository.FilmRepository;
import ru.hw.java13filmlibrary.filmLibrary.utils.FileHelper;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class FilmService
        extends GenericService<Film, FilmDTO> {

    private final DirectorRepository directorRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository repository,
                          FilmMapper filmMapper,
                          DirectorRepository directorRepository,
                          FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(repository, filmMapper);
        this.directorRepository = directorRepository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public FilmDTO addDirector(Long filmId, Long directorId) {
        Film film = repository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильм не найден с id=" + filmId));
        Director director = directorRepository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Режиссер не найден с id=" + directorId));
        film.getDirectors().add(director);
        return mapper.toDTO(repository.save(film));
    }

    public List<FilmWithDirectorsDTO> listAllFilmsWithDirectors() {
        return filmWithDirectorsMapper.toDTOs(repository.findAll());
    }

    public FilmDTO create(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public FilmDTO update(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
}
