package ru.hw.java13filmlibrary.filmLibrary.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.hw.java13filmlibrary.filmLibrary.dto.DirectorDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.DirectorWithFilmsDTO;
import ru.hw.java13filmlibrary.filmLibrary.service.DirectorService;

import java.util.List;

@Controller
@Hidden
@RequestMapping("/directors")
public class MVCDirectorController {

    private final DirectorService directorService;

    public MVCDirectorController(DirectorService directorService) {
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<DirectorWithFilmsDTO> directorWithFilmsDTOList = directorService.listAllDirectorsWithFilms();
        model.addAttribute("directors", directorWithFilmsDTOList);
        return "directors/viewAllDirectors";
    }

    @GetMapping("/add")
    public String create() {
        return "/directors/addDirector";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDTO directorDTO) {
        directorService.create(directorDTO);
        return "redirect:/directors";
    }
}
