package ru.hw.java13filmlibrary.filmLibrary.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.hw.java13filmlibrary.filmLibrary.dto.FilmDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.FilmWithDirectorsDTO;
import ru.hw.java13filmlibrary.filmLibrary.service.DirectorService;
import ru.hw.java13filmlibrary.filmLibrary.service.FilmService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
@Hidden
@RequestMapping("/films")
public class MVCFilmController {

    private final FilmService filmService;
    private final DirectorService directorService;

    public MVCFilmController(FilmService filmService,
                             DirectorService directorService) {
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<FilmWithDirectorsDTO> filmWithDirectorsDTO = filmService.listAllFilmsWithDirectors();
        model.addAttribute("films", filmWithDirectorsDTO);
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String create() {
        return "/films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO) {
        filmService.create(filmDTO);
        return "redirect:/films";
    }

    /**
     * Добавить режиссера к фильму - не получилось
     */
//    @GetMapping("/add-director/{filmId}")
//    public String addDirectorToFilm(@PathVariable Long filmId,
//                                    Model model) {
//        model.addAttribute("directors", directorService.listAll());
//        model.addAttribute("filmId", filmId);
//        model.addAttribute("film", filmService.getOne(filmId).getTitle());
//        return "/films/addDirectorToFilm";
//    }
//
//    @PostMapping("/add-director")
//    public String addDirectorToFilm(@ModelAttribute("filmDirectorForm") AddDirectorDTO addDirectorDTO) {
//        filmService.addDirector(addDirectorDTO);
//        return "redirect:/films";
//    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadFilm(@Param(value = "filmId") Long filmId) throws IOException {
        FilmDTO filmDTO = filmService.getOne(filmId);
        Path path = Paths.get(filmDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }
}