package ru.hw.java13filmlibrary.filmLibrary.repository;

import ru.hw.java13filmlibrary.filmLibrary.model.Order;

import java.util.List;

public interface OrderRepository
        extends GenericRepository<Order> {
    List<Order> findByUserId(Long user_id);
}
