package ru.hw.java13filmlibrary.filmLibrary.repository;

import org.springframework.stereotype.Repository;
import ru.hw.java13filmlibrary.filmLibrary.model.Director;

@Repository
public interface DirectorRepository
        extends GenericRepository<Director> {
}
