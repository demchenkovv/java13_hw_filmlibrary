package ru.hw.java13filmlibrary.filmLibrary.repository;

import ru.hw.java13filmlibrary.filmLibrary.model.User;

public interface UserRepository
        extends GenericRepository<User> {

//    select * from users where login = ?;
//    @Query(nativeQuery = true, value = "select * from users where login = :login")
    User findUserByLogin(String login);
    User findUserByEmail(String email);
}
