package ru.hw.java13filmlibrary.filmLibrary.repository;

import org.springframework.stereotype.Repository;
import ru.hw.java13filmlibrary.filmLibrary.model.Film;
// Аннотация @Repository - это специализация аннотации @Component, которая используется
// для указания того, что класс предоставляет механизм для хранения, извлечения,
// обновления, удаления и поиска объектов.
@Repository
public interface FilmRepository
        extends GenericRepository<Film> {
}
