package ru.hw.java13filmlibrary.filmLibrary.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(
        name = "default_generator",
        sequenceName = "order_seq",
        allocationSize = 1)
public class Order
        extends GenericModel {

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_USERS"))
    private User user;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(
            name = "film_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_FILMS"))
    private Film film;

    @Column(name = "rent_date")
    private LocalDate rentDate;

    @Column(name = "rent_period")
    private Integer rentPeriod;

    @Column(name = "purchase", nullable = false)
    private Boolean purchase;
}
