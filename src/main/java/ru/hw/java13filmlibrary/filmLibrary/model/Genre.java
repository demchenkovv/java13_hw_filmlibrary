package ru.hw.java13filmlibrary.filmLibrary.model;

public enum Genre {

    COMEDY("Комедия"),
    DOCUMENTARY("Документальный фильм"),
    SCIENCEFICTION("Научная фантастика"),
    FANTASY("Фэнтези"),
    CARTOON("Мультфильм"),
    DRAMA("Драма"),
    DETECTIVE("Детектив"),
    THRILLER("Триллер");

    private final String genreTextDisplay;

    Genre(String genreTextDisplay) {
        this.genreTextDisplay = genreTextDisplay;
    }

    public String getGenreTextDisplay() {
        return genreTextDisplay;
    }
}
