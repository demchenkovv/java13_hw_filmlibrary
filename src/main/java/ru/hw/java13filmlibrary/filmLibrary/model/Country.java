package ru.hw.java13filmlibrary.filmLibrary.model;

public enum Country {
    USSR("СССР"),
    US("Соединенные Штаты Америки"),
    CA("Канада");

    private String countryTextDisplay;

    Country(String countryTextDisplay) {
        this.countryTextDisplay = countryTextDisplay;
    }

    public String getCountryTextDisplay() {
        return countryTextDisplay;
    }
}
