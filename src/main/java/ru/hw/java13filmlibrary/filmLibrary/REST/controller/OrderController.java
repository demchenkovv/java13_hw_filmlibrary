package ru.hw.java13filmlibrary.filmLibrary.REST.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.hw.java13filmlibrary.filmLibrary.dto.OrderDTO;
import ru.hw.java13filmlibrary.filmLibrary.model.Order;
import ru.hw.java13filmlibrary.filmLibrary.service.OrderService;

@RestController
@RequestMapping(value = "/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
public class OrderController
        extends GenericController<Order, OrderDTO> {

    public OrderController(OrderService orderService) {
        super(orderService);
    }
}
