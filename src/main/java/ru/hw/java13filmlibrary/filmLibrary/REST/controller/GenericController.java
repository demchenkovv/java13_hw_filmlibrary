package ru.hw.java13filmlibrary.filmLibrary.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.hw.java13filmlibrary.filmLibrary.dto.GenericDTO;
import ru.hw.java13filmlibrary.filmLibrary.exception.MyDeleteException;
import ru.hw.java13filmlibrary.filmLibrary.model.GenericModel;
import ru.hw.java13filmlibrary.filmLibrary.service.GenericService;

import java.time.LocalDateTime;
import java.util.List;

/**
 * ВАЖНО! GenericController - должен быть АБСТРАКТНЫМ
 * <p>
 * Абстрактный контроллер
 * который реализует все EndPoint`ы для crud операций используя абстрактный репозиторий
 *
 * @param <E> - Entity с которой работает контроллер
 * @param <D> - DTO с которой работает контроллер
 */
@RestController
public abstract class GenericController<E extends GenericModel, D extends GenericDTO> {

    // protected final GenericRepository<E> genericRepository;
    private GenericService<E, D> service;


    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public GenericController(GenericService<E, D> service) {
        this.service = service;
    }

    @Operation(description = "Получить список всех записей", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<D>> getAll() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.listAll());
    }

    @Operation(description = "Получить данные по ID", method = "getOneById")
    @RequestMapping(value = "/getOneById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> getOneById(@RequestParam(value = "id") Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(service.getOne(id));
    }

    @Operation(description = "Создать данные", method = "created")
    @RequestMapping(value = "/created", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> created(@RequestBody D newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(service.create(newEntity));
    }

    @Operation(description = "Обновить данные", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> update(@RequestBody D updateEntity,
                                    @RequestParam(value = "id") Long id) {
        // Задаем сущности id, данные по которому хотим обновить
        updateEntity.setId(id);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(service.update(updateEntity));
    }

    //@RequestParam: localhost:9090/api/rest/books/deleteBook?id=1
    //@PathVariable: localhost:9090/api/rest/books/deleteBook/1
    @Operation(description = "Удалить запись по ID", method = "delete")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) throws MyDeleteException {
        service.delete(id);
    }
}
