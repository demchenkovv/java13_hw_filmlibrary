package ru.hw.java13filmlibrary.filmLibrary.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.hw.java13filmlibrary.filmLibrary.dto.DirectorDTO;
import ru.hw.java13filmlibrary.filmLibrary.model.Director;
import ru.hw.java13filmlibrary.filmLibrary.service.DirectorService;

@RestController
@RequestMapping("/directors") // localhost:8080/directors
@Tag(name = "Режиссеры", description = "Контроллер для работы с режиссерами фильмотеки.")
public class DirectorController
        extends GenericController<Director, DirectorDTO> {

    private DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }

    @Operation(description = "Добавить фильм к режиссеру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "director_id") Long directorId,
                                               @RequestParam(value = "film_id") Long filmId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(directorService.addFilm(directorId, filmId));
    }
}
