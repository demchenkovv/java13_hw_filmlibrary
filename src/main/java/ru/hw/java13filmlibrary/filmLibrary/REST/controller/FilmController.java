package ru.hw.java13filmlibrary.filmLibrary.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.hw.java13filmlibrary.filmLibrary.dto.FilmDTO;
import ru.hw.java13filmlibrary.filmLibrary.model.Film;
import ru.hw.java13filmlibrary.filmLibrary.service.FilmService;

@RestController
@RequestMapping(value = "/films") // localhost:8080/films
@Tag(name = "Фильмы", description = "Контроллер по работе с фильмами фильмотеки.")
public class FilmController
        extends GenericController<Film, FilmDTO> {

    private FilmService filmService;

    public FilmController(FilmService filmService) {
        super(filmService);
        this.filmService = filmService;
    }

    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "film_id") Long filmId,
                                               @RequestParam(value = "director_id") Long directorId) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(filmService.addDirector(filmId, directorId));
    }

//    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Film> addDirector(@RequestParam(value = "director_id") Long directorId,
//                                            @RequestParam(value = "film_id") Long filmId) {
//
//        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссер не найден по заданному id=" + directorId));
//        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден по заданному id=" + filmId));
//
//        film.getDirectors().add(director);
//
//        return ResponseEntity
//                .status(HttpStatus.CREATED)
//                .body(filmRepository.save(film));
//    }
}
