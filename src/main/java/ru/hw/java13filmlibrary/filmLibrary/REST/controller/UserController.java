package ru.hw.java13filmlibrary.filmLibrary.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.hw.java13filmlibrary.filmLibrary.config.jwt.JWTTokenUtil;
import ru.hw.java13filmlibrary.filmLibrary.dto.FilmDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.LoginDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.OrderDTO;
import ru.hw.java13filmlibrary.filmLibrary.dto.UserDTO;
import ru.hw.java13filmlibrary.filmLibrary.model.User;
import ru.hw.java13filmlibrary.filmLibrary.service.OrderService;
import ru.hw.java13filmlibrary.filmLibrary.service.UserService;
import ru.hw.java13filmlibrary.filmLibrary.service.userdetails.CustomUserDetailsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
@Slf4j
public class UserController
        extends GenericController<User, UserDTO> {

    private final UserService userService;
    private OrderService orderService;
    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;


    public UserController(UserService userService,
                          OrderService orderService,
                          CustomUserDetailsService customUserDetailsService,
                          JWTTokenUtil jwtTokenUtil) {
        super(userService);
        this.userService = userService;
        this.orderService = orderService;
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Operation(description = "Взять фильм в аренду", method = "rentFilm")
    @RequestMapping(value = "/rentFilm", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> rentFilm(@RequestParam(value = "user_id") Long userId,
                                             @RequestParam(value = "film_id") Long filmId,
                                             @RequestParam(value = "rent_period") Integer rentPeriod) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(orderService.rentFilm(userId, filmId, rentPeriod));
    }

    @Operation(description = "Купить фильм", method = "buyFilm")
    @RequestMapping(value = "/buyFilm", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> buyFilm(@RequestParam(value = "user_id") Long userId,
                                            @RequestParam(value = "film_id") Long filmId) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(orderService.buyFilm(userId, filmId));
    }

    @Operation(description = "Список всех когда-либо арендованных/купленных фильмов у пользователя", method = "getAllFilmsByUserId")
    @RequestMapping(value = "/getAllFilmsByUserId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FilmDTO>> getAllFilmsByUserId(@RequestParam(value = "user_id") Long userId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(orderService.getAllFilmsByUserId(userId));
    }

    @Operation(description = "Список всех ДЕЙСТВУЮЩИХ арендованных или купленных фильмов у пользователя", method = "getCurrentFilmsByUserId")
    @RequestMapping(value = "/getCurrentFilmsByUserId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FilmDTO>> getCurrentFilmsByUserId(@RequestParam(value = "user_id") Long userId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(orderService.getCurrentFilmsByUserId(userId));
    }

    // Авторизация с использованием JWT
    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        Map<String, Object> response = new HashMap<>();
        log.info("LoginDTO: {}", loginDTO);
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        log.info("foundUser, {}", foundUser);
        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\nНеверный пароль");
        }
        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("authorities", foundUser.getAuthorities());
        return ResponseEntity.ok().body(response);
    }
}
