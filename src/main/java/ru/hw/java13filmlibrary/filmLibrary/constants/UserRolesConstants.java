package ru.hw.java13filmlibrary.filmLibrary.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String MODERATOR = "MODERATOR";
    String USER = "USER";
}
