package ru.hw.java13filmlibrary.filmLibrary.constants;

public interface FileDirectoriesConstants {
    String FILMS_UPLOAD_DIRECTORY = "files/films";
}
