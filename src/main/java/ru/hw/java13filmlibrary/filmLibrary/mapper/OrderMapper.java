package ru.hw.java13filmlibrary.filmLibrary.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.hw.java13filmlibrary.filmLibrary.dto.OrderDTO;
import ru.hw.java13filmlibrary.filmLibrary.model.Order;
import ru.hw.java13filmlibrary.filmLibrary.repository.FilmRepository;
import ru.hw.java13filmlibrary.filmLibrary.repository.UserRepository;

import java.util.Set;

@Component
public class OrderMapper
        extends GenericMapper<Order, OrderDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;

    protected OrderMapper(ModelMapper modelMapper,
                          UserRepository userRepository,
                          FilmRepository filmRepository) {
        super(modelMapper, Order.class, OrderDTO.class);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId())
                .orElseThrow(() -> new NotFoundException("Фильм не найден")));
        destination.setUser(userRepository.findById(source.getUserId())
                .orElseThrow(() -> new NotFoundException("Пользователь не найден")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setFilmId(source.getFilm().getId());
        destination.setUserId(source.getUser().getId());
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен для данного класса");
    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .setPostConverter(toDtoConverter());
        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .setPostConverter(toEntityConverter());
    }
}
