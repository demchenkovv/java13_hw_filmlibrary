package ru.hw.java13filmlibrary.filmLibrary.mapper;

import ru.hw.java13filmlibrary.filmLibrary.dto.GenericDTO;
import ru.hw.java13filmlibrary.filmLibrary.model.GenericModel;

import java.util.List;

/**
 * Класс для преобразования из Энтити в ДТО и наоборот
 *
 * @param <E> наш Entity
 * @param <D> наш DTO
 */
public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);

    D toDTO(E entity);

    List<D> toDTOs(List<E> entities);

}
