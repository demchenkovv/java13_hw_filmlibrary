package ru.hw.java13filmlibrary.filmLibrary.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.hw.java13filmlibrary.filmLibrary.dto.UserDTO;
import ru.hw.java13filmlibrary.filmLibrary.model.GenericModel;
import ru.hw.java13filmlibrary.filmLibrary.model.User;
import ru.hw.java13filmlibrary.filmLibrary.repository.OrderRepository;
import ru.hw.java13filmlibrary.filmLibrary.utils.DateFormatter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper
        extends GenericMapper<User, UserDTO> {

    private final OrderRepository orderRepository;

    protected UserMapper(ModelMapper modelMapper,
                         OrderRepository orderRepository) {
        super(modelMapper, User.class, UserDTO.class);

        this.orderRepository = orderRepository;
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getOrders())) {
            destination.setOrders(new HashSet<>(orderRepository.findAllById(source.getOrders())));
        } else {
            destination.setOrders(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrders(getIds(source));
    }

    @Override
    protected Set<Long> getIds(User entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());

    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .setPostConverter(toEntityConverter());
    }
}
