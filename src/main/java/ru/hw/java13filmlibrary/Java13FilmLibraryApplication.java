package ru.hw.java13filmlibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java13FilmLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(Java13FilmLibraryApplication.class, args);
    }

}
